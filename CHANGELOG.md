# Changelog
All notable changes to this project will be documented in this file.

## [1.1.0] - 2024-08-07

### Added

- CI to enforce linting (#9)
- Code linting (#9)
- requirements.txt for linting (#9)

### Changed

- Refactor corner points arrays for readability

## [1.0.3] - 2024-08-06

### Added

- Table of contents in readme

### Changed

- Active voice in readme (#6)
- Fixed 404'd image in readme (#4)
- Type hint style from 3.6 to 3.9 (#8)
- Use more appropriate example in readme to show duplication technique (#7)

## [1.0.2] - 2021-02-07

### Changed

- Tmp directory gitignore fixed - directory now present; all files inside are ignored (#3)

## [1.0.1] - 2021-02-06

### Added

- Images in the readme and more questions
- Sample images and configurations

## [1.0.0] - 2021-02-06

### Added

- Changelog
- License
- Program
- Readme
- Template files
