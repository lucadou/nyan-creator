# Nyan Cat Creator

![A Nyan Cat GIF, faithful to the original, that was generated with this program](samples/nyan-og.gif)

Remember [Nyan Cat](https://www.youtube.com/watch/QH2-TGUlwu4)?
Have you ever wanted to make own with a different color trail?
Well, now you can!

{:toc}

## Setup

This has been tested on Ubuntu 22.04 using Python 3.10, but it should work
on any distro with the equivalent ImageMagick package and any Python 3
release with `subprocess.run` (which is 3.5+).

```bash
sudo apt install imagemagick
```

Please note that, while the `graphicsmagick-imagemagick-compat` package
[claims](https://packages.debian.org/sid/graphicsmagick-imagemagick-compat)
to be a drop-in replacement for ImageMagick,
it is not compatible because it lacks the following operators:

* `-layers flatten` ([docs](https://imagemagick.org/script/command-line-options.php#layers))
  * GraphicsMagick has [an equivalent operator](http://www.graphicsmagick.org/GraphicsMagick.html#details-flatten),
    `-flatten`, but this is a legacy operator in ImageMagick which may be deprecated.
* `-layers optimize` ([docs](https://imagemagick.org/script/command-line-options.php#layers))
  * Does not appear to have a direct equivalent in GraphicsMagick.

### Dependency Installation

This application only uses the Python stdlib and interfaces with ImageMagick
via its CLI; no additional packages are necessary unless you want to develop it.

If you want to tinker with the project:

```bash
sudo apt install python3.10-venv
python3 -m venv venv
source venv/bin/activate
pip install -r requirements/dev.txt
```

## Configuration

Before you run the application, you will need to prepare a color file. There
is a sample file, [example.json](src/example.json), which you can look at.

The structure of the file is incredible simple. Just add the hex codes of the
colors you want into the `colors` array.

To adjust the relative size of the stripes, just duplicate colors. For example,
this file:

```json
{
  "colors": [
    "#d60270",
    "#d60270",
    "#9b4f96",
    "#0038a8",
    "#0038a8"
  ]
}
```

Would produce this image:

![Nyan Cat with a color trail matching the bisexual pride flag](samples/nyan-bisexual.gif)

## Running

To run the application, you must supply two args:
the colors file and the output file.

If the output file already exists, it will be overwritten.
The application will warn you if you are attempting to use an
empty/non-existent colors file or a colors file with too many colors.

```bash
./nyan_creator.py <colorsfile.json> <outfile.gif>
# Alternatively:
python3 nyan_creator.py <colorsfile.json> <outfile.gif>
```

## Notes/FAQ

> Does this fully reproduce the original Nyan Cat GIF?

It does not. It comes close; however, if you look closely, there is one notable
difference:

![Comparison of the original Nyan Cat with a generated version. The generated version lacks a couple hanging pixels](samples/nyan-og-comparison.png)

This comparison is on the first frame of the GIFs, but it occurs in several frames.
The reason we did not attempt to correct for this is because not every
row has this little bump - attempting to come up with a concise algorithm for
any number of colors would be impossible unless we knew the formula for the
original Nyan Cat (which probably doesn't even exist).

Other than that, it is the same.
And even if you are scrutinizing it, the discrepancy is not very easy to spot.

> How does this work?

It divides the specified colors into approximately-equally sized lines and draws
rectangles onto the canvas images.
It then overlays the template images to add the cat and background.
Finally, it puts them together into a GIF.

> How many colors can I use?

Technically, you can add up to 83 different colors.
However, after around 40 colors, they start to overlap due to limitations of
ImageMagick's rectangle drawing capabilities.
For optimal viewability, we recommend using fewer than 10 colors.
The program will warn you at both of these thresholds.

The program will refuse to run with more than 83 colors since some colors would be not viewable.
See the [color_tests folder](samples/color_tests) for samples of generated GIFs with large numbers of colors.

Even if you use a bunch of colors, it will not hit the 255 GIF color limit.
This is because there are only 13 colors used by the cat and background:

```python
{'#003366FF', '#CFCF2500', '#00000000', '#30300000', '#FE379DFF', '#FDCF9CFF',
 '#FF99FDFF', '#9C9C9DFF', '#E09A92FF', '#FB0302FF', '#F7F9FAFF', '#676768FF',
 '#030404FF'}
```

This was computed by running this command on every file:

```bash
convert <file>.png -define histogram:unique-colors=true -format %c histogram:info:- | awk -F '#' '{print $2}' | awk '{print $1}' >> colors.txt
```

The resulting color list was then de-duplicated with Python:

```python
with open('colors.txt', 'r') as f:
  color_list = f.readlines()
colors = set(color_list)
print(colors)
```

> Why not use an ImageMagick module for Python?

None of the ImageMagick bindings were both decent and comprehensive.
That is, [Wand](https://docs.wand-py.org/en/0.6.5/) looked like the best,
most Pythonic module, but it did not have any animation commands.
Due to an inability to do everything with the same bindings,
the easiest way was to just use the CLI.
Plus, the commands in use are not super complex.

> How did you develop this process?

* We took a [Nyan Cat GIF](https://newscrewdriver.files.wordpress.com/2018/10/poptartcat320240.gif)
  and used [ezGIF](https://ezgif.com/split) to split it into individual frames
  using the "Redraw every frame with details from previous frames" option.
* We then used GIMP to isolate the cat, background, and color trail.
  We also used GIMP to locate the corners of each rectangle we would need to draw.
  You can view the GIMP files and the corner calculations in
  [src/originals](src/originals).
* Finally, we exported each frame (minus the color trail) as the template,
  and an equivalent number of blank frames for the canvas.

## Versioning

We use Semantic Versioning, aka [SemVer](https://semver.org/spec/v2.0.0.html).

## Authors

* Luna Lucadou - Wrote the application in her free time

## Acknowledgements

* Christopher Torres, aka prguitarman, who created
  [the original GIF](https://www.webcitation.org/6AX4J3pMz?url=http://www.prguitarman.com/index.php?id=348)
  on April 2, 2011
* Sara, aka "saraj00n", who combined the GIF with the song and uploaded
  [the original video](https://www.youtube.com/watch/QH2-TGUlwu4)
  on April 5, 2011
* Luna's good friend Jade who showed her
  [this meme](https://web.archive.org/web/20210109060752/https://pbs.twimg.com/media/ErRNN4TW4AEgm4P.jpg)
  which inspired the creation of this application

## License

This project is licensed under the GNU Affero General Public License v3.0 - see the
[LICENSE](LICENSE.md) file for details.

## Bugs, feature ideas, etc?

Feel free to fill out an issue or submit a merge request!
