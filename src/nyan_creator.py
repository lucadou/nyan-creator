#!/usr/bin/python3
"""
This file is part of Nyan Cat Creator, a custom Nyan Cat creator.
Copyright (C) 2021-2024 Luna Lucadou

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import json
import os
import subprocess
import sys
from typing import NamedTuple

# Exit codes (source: https://www.freebsd.org/cgi/man.cgi?query=sysexits)
STATUS_ARGS_ERR: int = 64  # EX_USAGE
STATUS_COLORS_EMPTY: int = 65  # EX_DATAERR - no colors
STATUS_COLORS_EXCESSIVE: int = 65  # EX_DATAERR - too many colors
STATUS_COLORS_MISSING: int = 66  # EX_NOINPUT - colors file does not exist
STATUS_TEMPLATE_MISSING: int = 69  # EX_UNAVAILABLE
STATUS_MAGICK_MISSING: int = 69  # EX_UNAVAILABLE
STATUS_MAGICK_ERROR: int = 70  # EX_SOFTWARE
# Directories
CANVAS_DIRECTORY: str = "canvas"
OVERLAY_DIRECTORY: str = "overlays"
TMP_DIRECTORY: str = "tmp"


class Corner(NamedTuple):
    """
    A wrapper for an X/Y coordinate pair.
    """

    x: int
    y: int


class Frame(NamedTuple):
    """
    A wrapper for a specific group of corners which will be
    referenced in a specific frame.
    """

    corners: list[Corner]


# Boundaries
# Format: [
#   [Frame 0 [Corner 1 X,Y], [Corner 2 X,Y], ...],
#   [Frame 1 [Corner 1 X,Y], [Corner 2 X,Y], ...],
#    ...
# ]
BOUNDS_UPPER_LEFT: list[Frame] = [
    Frame([Corner(0, 83), Corner(14, 79), Corner(50, 83), Corner(87, 79)]),  # Frame 0
    Frame([Corner(0, 83), Corner(14, 79), Corner(50, 83), Corner(87, 79)]),  # Frame 1
    Frame([Corner(0, 79), Corner(10, 83), Corner(46, 79), Corner(82, 83)]),  # Frame 2
    Frame([Corner(0, 79), Corner(9, 83), Corner(46, 79), Corner(82, 83)]),  # Frame 3
    Frame([Corner(0, 83), Corner(14, 79), Corner(50, 83), Corner(87, 79)]),  # Frame 4
    Frame([Corner(0, 83), Corner(14, 79), Corner(50, 83), Corner(87, 79)]),  # Frame 5
    Frame([Corner(0, 79), Corner(9, 83), Corner(46, 79), Corner(82, 83)]),  # Frame 6
    Frame([Corner(0, 79), Corner(9, 83), Corner(46, 79), Corner(82, 83)]),  # Frame 7
    Frame([Corner(0, 83), Corner(14, 79), Corner(50, 83), Corner(87, 79)]),  # Frame 8
    Frame([Corner(0, 83), Corner(14, 79), Corner(50, 83), Corner(87, 79)]),  # Frame 9
    Frame([Corner(0, 79), Corner(9, 83), Corner(46, 79), Corner(82, 83)]),  # Frame 10
    Frame([Corner(0, 79), Corner(9, 83), Corner(46, 79), Corner(82, 83)]),  # Frame 11
]
BOUNDS_LOWER_RIGHT: list[Frame] = [
    Frame([Corner(13, 165), Corner(49, 161), Corner(86, 165), Corner(118, 161)]),  # F0
    Frame([Corner(13, 165), Corner(49, 161), Corner(86, 165), Corner(118, 161)]),  # F1
    Frame([Corner(9, 162), Corner(45, 165), Corner(81, 161), Corner(118, 165)]),  # F2
    Frame([Corner(9, 161), Corner(45, 165), Corner(81, 161), Corner(118, 165)]),  # F3
    Frame([Corner(13, 165), Corner(49, 161), Corner(86, 165), Corner(122, 161)]),  # F4
    Frame([Corner(13, 165), Corner(49, 161), Corner(86, 165), Corner(122, 161)]),  # F5
    Frame([Corner(9, 161), Corner(45, 165), Corner(81, 161), Corner(113, 165)]),  # F6
    Frame([Corner(9, 161), Corner(45, 165), Corner(81, 161), Corner(113, 165)]),  # F7
    Frame([Corner(13, 165), Corner(49, 161), Corner(86, 165), Corner(122, 161)]),  # F8
    Frame([Corner(13, 165), Corner(49, 161), Corner(86, 165), Corner(122, 161)]),  # F9
    Frame([Corner(9, 161), Corner(45, 165), Corner(81, 161), Corner(118, 165)]),  # F10
    Frame([Corner(9, 161), Corner(45, 165), Corner(81, 161), Corner(118, 165)]),  # F11
]
# Metadata
ALPHA_ENABLED: bool = False  # Disable transparency to increase colorspace.
IMAGE_COMMENT: str = (
    "Made with Luna Lucadou's Nyan Cat Creator - https://gitlab.com/lucadou/nyan-creator"
)
# Numbers
FRAME_DELAY: int = 7  # In hundredth of a second; it was 0.07s in the original GIF.
MAX_RECOMMENDED_COLORS: int = 10
MAX_VIEWABLE_COLORS: int = 40
NUM_COLUMNS: int = 4  # Number of rectangles we have to draw per color.
# Should match # of corners per row in the BOUNDS_ lists.
NUM_FRAMES: int = 12  # Should match # of files in canvas/ and overlays/ folder.
TRAIL_HEIGHT: int = 83  # Pixel height of the color trail.
# Technically some areas are >83 pixels, but because of how we draw the trail,
# we can use the same 83 pixels everywhere.


def load_file(filename: str) -> list[str]:
    """
    Given a filename, loads the file and extracts the array of colors.

    :param filename: the name/path of the file to load
    :return: the colors listed in the file
    """
    if os.path.isfile(filename):
        with open(filename, "r", encoding="utf-8") as f:
            try:
                colors: list[str] = json.loads(f.read())["colors"]
                return colors
            except KeyError:
                sys.stderr.write("Error: invalid colors file format\n")
                sys.exit(STATUS_COLORS_EMPTY)
    else:
        sys.stderr.write(f'Error: unable to locate file "{filename}"\n')
        sys.exit(STATUS_COLORS_MISSING)


def execute_im_cmd(cmd: str) -> None:
    """
    Executes a given ImageMagick command. If the command results in an error,
    it outputs the error and exits the program

    :param cmd: the command to execute
    :return: nothing
    """
    proc: subprocess.CompletedProcess | subprocess.CompletedProcess[bytes] = (
        subprocess.run(cmd, capture_output=True, shell=True, check=False)
    )
    try:
        proc.check_returncode()
    except subprocess.CalledProcessError:
        sys.stderr.write("Error executing the following command:\n")
        sys.stderr.write(f'  "{cmd}"\n')
        sys.stderr.write(f'Stdout: "{proc.stdout.decode()}"\n')
        sys.stderr.write(f'Stderr: "{proc.stderr.decode()}"\n')
        sys.exit(STATUS_MAGICK_ERROR)


def check_colors(color_list: list[str]) -> None:
    """
    Given a list of colors, determines if the number of colors present poses a
    potential problem. This is a very basic function; it does not account for
    colors repeating multiple times in the same location, but it gives a good
    guess as to whether the design will cause problems

    :param color_list: the list of colors
    :return: nothing
    """
    if len(color_list) == 0:
        sys.stderr.write("Error: no colors specified\n")
        sys.exit(STATUS_COLORS_EMPTY)
    elif len(color_list) > 83:
        sys.stderr.write("Error: too many colors specified\n")
        sys.exit(STATUS_COLORS_EXCESSIVE)
    elif len(color_list) >= MAX_VIEWABLE_COLORS:
        sys.stderr.write(
            f"Warning: it is recommended that you use less than {str(MAX_VIEWABLE_COLORS)}"
            " colors to avoid overlapping; some colors may not be"
            " visible\n"
        )
    elif len(color_list) >= MAX_RECOMMENDED_COLORS:
        sys.stderr.write(
            f"Warning: it is recommended that you use less than "
            f"{str(MAX_RECOMMENDED_COLORS)} colors to ensure visibility\n"
        )


def check_save_name(save_to: str) -> None:
    """
    Checks to see if a filename can probably be saved to. Note that this does
    *not* check for any file locks or anything, it just makes sure the save
    name ends in ".gif" since ImageMagick seems to rely on the extension

    :param save_to: the file name to check
    :return: nothing
    """
    if not save_to.endswith(".gif"):
        sys.stderr.write('Error: file name must end in ".gif"\n')
        sys.exit(STATUS_ARGS_ERR)


def check_imagemagick() -> None:
    """
    Checks to see if ImageMagick is installed and exits if it is not.
    Will also exit if only GraphicsMagick is detected.

    :return: nothing
    """
    gm_installed: bool = False
    try:
        subprocess.run("gm -version".split(), capture_output=True, check=True)
        gm_installed = True
    except FileNotFoundError:
        pass

    try:
        subprocess.run("convert --version".split(), capture_output=True, check=True)
        # -version works on both IM and GM, while --convert only works on IM,
        # allowing us to easily detect incompatible software early on.
        # If they have GM installed but this command succeeds,
        # they must have IM installed alongside GM but not GM's IM-compatibility layer,
        # so the application can run like normal.
    except FileNotFoundError:
        sys.stderr.write("Error: ImageMagick is not installed\n")
        if gm_installed:
            sys.stderr.write("You appear to have GraphicsMagick installed\n")
            sys.stderr.write(
                "GraphicsMagick is not supported; please install ImageMagick\n"
            )
        sys.exit(STATUS_MAGICK_MISSING)


def determine_color_heights(num_colors: int) -> list[int]:
    """
    Given the number of colors present, determines how many pixels each one
    will take up vertically

    :param num_colors: the number of colors
    :return: a list of how high each color should be
    """
    base_height: int = int(TRAIL_HEIGHT / num_colors)
    leftover_height: int = TRAIL_HEIGHT % num_colors
    color_heights: list[int] = [base_height] * num_colors

    if leftover_height > 0:
        for i in range(0, num_colors, 2):
            # If there is some leftover height,
            # we increment every other row's height to try to add some variety
            if leftover_height > 0:
                color_heights[i] += 1
                leftover_height -= 1
        for i in range(num_colors - 1, 0, -2):
            # The reason we do this one in reverse is because, on the original
            # Nyan Cat GIF, the row heights are (from top to bottom):
            # 14, 13, 14, 14, 14, 14
            # So it makes sense to try to keep the even-numbered
            # rows towards the top shorter.
            # Also, having 0 as the inclusive ending boundary is fine since
            # it excludes the second from the top row, and you cannot have
            # leftover_height == num_colors or this branch never would have
            # been taken to begin with, as the modulo would have been 0.
            if leftover_height > 0:
                color_heights[i] += 1
                leftover_height -= 1

    return color_heights


def paint_canvas(color_list: list[str]) -> list[str]:  # pylint: disable=too-many-locals
    """
    Given a list of colors, paints the appropriate rectangles onto each canvas

    :param color_list: the list of colors to use
    :return: a list of the files that were painted in
    """
    num_colors: int = len(color_list)
    # Determine appropriate size of each color
    color_heights: list[int] = determine_color_heights(num_colors)
    painted_files: list[str] = []

    # Paint colors onto each canvas
    for f in range(NUM_FRAMES):
        frame_ul: Frame = BOUNDS_UPPER_LEFT[f]
        frame_lr: Frame = BOUNDS_LOWER_RIGHT[f]
        padded_frame_num = str(f)
        if len(padded_frame_num) == 1:
            padded_frame_num = "0" + padded_frame_num
        input_name: str = f"{CANVAS_DIRECTORY}/{padded_frame_num}.png"
        output_name: str = f"{TMP_DIRECTORY}/painted_{padded_frame_num}.png"
        for c in range(NUM_COLUMNS):
            col_ul: Corner = frame_ul.corners[c]
            col_lr: Corner = frame_lr.corners[c]
            curr_height: int = col_ul.y  # Upper left Y
            for i in range(num_colors):
                next_height: int = curr_height + color_heights[i] - 1
                rect_bounds: list[int] = [
                    col_ul.x,
                    curr_height,
                    col_lr.x,
                    next_height,
                ]
                # Rectangle UL X, UL Y, LR X, LR Y
                cmd: str = (
                    f"convert {input_name} -fill '{color_list[i]}' -draw "
                    f"'rectangle {str(rect_bounds[0])} {str(rect_bounds[1])} "
                    f"{str(rect_bounds[2])} {str(rect_bounds[3])}' {output_name}"
                )
                execute_im_cmd(cmd)

                curr_height = next_height + 1
                input_name = output_name
                # We set input_name to output_name because we want the same file
                # to be modified multiple times since we cannot draw multiple
                # rectangles of different colors in the same command.
        painted_files.append(output_name)
    return painted_files


def overlay_bgs(canvas_list: list[str]) -> list[str]:
    """
    Given a list of frames with the color trail drawn, overlays the background
    on each file and return a list of the new files.
    The canvas list *MUST* be in sequential order.

    :param canvas_list: the list of files to overlay the background onto
    :return: a list of files with the overlay
    """
    out_files: list[str] = []
    for i in range(NUM_FRAMES):
        padded_frame_num = str(i)
        if len(padded_frame_num) == 1:
            padded_frame_num = "0" + padded_frame_num
        bg_name: str = f"{OVERLAY_DIRECTORY}/{padded_frame_num}.png"
        trail_name: str = f"{canvas_list[i]}"
        output_name: str = f"{TMP_DIRECTORY}/merged_{padded_frame_num}.png"
        cmd: str = (
            f"convert {trail_name} {bg_name} -gravity center -background None "
            f"-layers flatten {output_name}"
        )
        execute_im_cmd(cmd)

        out_files += [output_name]
    return out_files


def animate(file_list: list[str], save_to: str, delay: float = FRAME_DELAY):
    """
    Given a list of files, animates them with the given delay.
    If there are any errors, the program will exit.

    The delay is based on https://imagemagick.org/script/command-line-options.php#delay
    (defaults to "ticks", which is a hundredth of a second).

    :param file_list: the list of files to animate
    :param save_to: the location to save the file to
    :param delay: the delay between each frame, in hundredths of a second
    :return: nothing
    """
    cmd = f'convert -comment "{IMAGE_COMMENT}" -delay {str(delay)} '
    # Using -comment at the start is equivalent to using "-set comment {}"
    # after the files are all listed (the order is important).
    # The comment is likely to be erased most of the time since
    # so many websites scrub metadata and re-compress images, but oh well.
    if not ALPHA_ENABLED:
        cmd += "-alpha remove "

    # Add in the images
    for f in file_list:
        cmd += f"{f} "
    cmd += f"-loop 0 -layers optimize {save_to}"

    execute_im_cmd(cmd)


def main(color_file: str, output_file: str) -> None:
    """
    Creates a Nyan Cat at the specified location using the specified colors file.

    :param color_file: the colors file to read
    :param output_file: the location to save the GIF
    :return: None
    """
    check_imagemagick()
    colors: list[str] = load_file(color_file)
    check_colors(colors)
    check_save_name(output_file)
    print("Generating color trail...")
    painted_files: list[str] = paint_canvas(colors)
    print("Overlaying background and cat...")
    overlaid_files: list[str] = overlay_bgs(painted_files)
    print("Animating...")
    animate(overlaid_files, output_file)
    print("Animation created")
    print(f"Saved image to {output_file}")


if __name__ == "__main__":
    if len(sys.argv) != 3:
        sys.stderr.write(f"usage: {sys.argv[0]} <colors_file> <output_file>\n")
        # If the user does "./nyan_creator.py", that gets stored in argv[0],
        # but "python3 nyan_creator.py" also stores "nyan_creator.py" in argv[0],
        # so we do not need any special checks for how exactly it was called.
        sys.exit(STATUS_ARGS_ERR)

    main(sys.argv[1], sys.argv[2])
